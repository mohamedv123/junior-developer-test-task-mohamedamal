<?php
require_once 'product.php';
require_once  'connection.php';

class startConnection
{
    public $id  = '';
    public function __construct($db,$method)
    {
        $this->db = $db;
        $this->method = $method;
    }

    function resultView($result, $method, $con, $id)
    {
        if (!$result) {
            http_response_code(404);
            die(mysqli_error($con));
            echo 'result is for delete';
        }
        if ($method == 'GET') {     
            if (!$id) echo '[';
            for ($i = 0; $i < mysqli_num_rows($result); $i++) {
                echo ($i > 0 ? ',' : '') . json_encode(mysqli_fetch_object($result));
            }
            if (!$id) echo ']';
        } elseif ($method == 'POST') {
            echo json_encode($result);
        } else {
            echo mysqli_affected_rows($con);
        }
    }

    function startQuery()
    {
        switch ($this->method) {
            case 'GET':
                $this->resultView($this->db->read(), $this->method, $this->db->con, $this->id);
                break;
            case 'POST':
                $this->resultView($this->db->insert(), $this->method, $this->db->con, $this->id);
                break;
            case 'DELETE':
                $this->resultView($this->db->delete(), $this->method, $this->db->con, $this->id);
                break;
        }
    }

    function closeConnection()
    {
        $this->db->con->close();
    }
}
