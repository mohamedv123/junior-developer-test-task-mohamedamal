<?php
include_once 'product.php';

class Database{
	
	//local 
	// private $servername = "localhost";
	// private $username   = "root";
	// private $password   = "";
	// private $database   = "test_db";

	//remote 
	private $servername = "fdb29.awardspace.net";
	private $database   = "3660903_productsproject"; 
	private $username   = "3660903_productsproject";
	private $password   = "qqqqwwww11112222"; 

	public $con;
	public $id = '';

	function __construct(){
		$this->connect_db();
	}

	public function connect_db(){
		$this->con = new mysqli($this->servername, $this->username,$this->password,$this->database);
		if(mysqli_connect_error()){
			die("Database Connection Failed" . mysqli_connect_error() . mysqli_connect_errno());
		}
	}

	public function read(){										
		if (isset($_GET["id"])) {
			$this->id = $_GET['id'];
		}
		$sql = "select * from products" . ($this->id ? " where id=$this->id" : '');
		$result = mysqli_query($this->con, $sql);
		return $result;
	}

	public function insert(){   
		$sku = strval($_POST["sku"]);
		$name = strval($_POST["name"]);
		$price = floatval($_POST["price"]);
		$type = strval($_POST["type"]);
		$size = floatval($_POST["size"]);
		$height = floatval($_POST["height"]);
		$width = floatval($_POST["width"]);
		$length = $_POST["length"];
		$weight = floatval($_POST["weight"]);
		$product = new Product($sku,$name,$price,$type);

		if($product->productValidation($sku,$name,$price,$type) == 'ready'){
			$sql = "INSERT INTO products (SKU, Name, Price, Type, Size, Height, Width, Length, Weight) values ('$sku', '$name', '$price', '$type', '$size','$height','$width','$length','$weight')";
			$result = mysqli_query($this->con, $sql);
			return $result;
		}else {
			echo 'Errors found, Please fill in the missing inputs';
			return false;
		} 
	}

	public function delete(){									
		if (isset($_GET['delete'])) {
			$delete =  $_GET['delete'];
			$integerIDs = array_map('intval', explode(',', $delete));
			$sql = 'DELETE FROM `products` WHERE `id` IN (' . implode(',', $integerIDs) . ')';
		}
		$result = mysqli_query($this->con, $sql);
		return $result;
	}
}
$database = new Database();
