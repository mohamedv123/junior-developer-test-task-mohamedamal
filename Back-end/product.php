<?php
class Product
{
  public $sku;
  public $name;
  public $price;
  public $type;
  private $errors = [];

  public function __construct($sku, $name, $price, $type)
  {
    $this->sku = $sku;
    $this->name = $name;
    $this->price = $price;
    $this->type = $type;
  }

  public function productValidation($sku, $name, $price, $type)
  {
    if (empty($sku)) {
      $this->errors['sku'] = 'sku is empty';
    }
    if (empty($name)) {
      $this->errors['name'] = 'name is empty';
    }
    if (empty($price)) {
      $this->errors['price'] = 'price is empty';
    }
    if (empty($type)) {
      $this->errors['type'] = 'type is empty';
    }
    if (count($this->errors) > 0) {
      return $this->errors;
    } else return 'ready';
  }
}

class Dvd extends Product
{
  public $size;
  private $errors = [];

  public function __construct($sku, $name, $price, $type, $size)
  {
    $this->sku = $sku;
    $this->name = $name;
    $this->price = $price;
    $this->type = $type;
    $this->size = $size;
  }

  public function validation($sku, $name, $price, $type, $size)
  {
    if (empty($sku)) {
      $this->errors['sku'] = 'sku is empty';
    }
    if (empty($name)) {
      $this->errors['name'] = 'name is empty';
    }
    if (empty($price)) {
      $this->errors['price'] = 'price is empty';
    }
    if (empty($type)) {
      $this->errors['type'] = 'type is empty';
    }
    if (empty($size)) {
      $this->errors['size'] = 'size is empty';
    }
    if (count($this->errors) > 0) {
      return $this->errors;
    } else return 'ready';
  }
}



class Furniture extends Product
{
  public $height;
  public $width;
  public $length;
  private $errors = [];

  public function __construct($sku, $name, $price, $type, $height, $width, $length)
  {
    $this->sku = $sku;
    $this->name = $name;
    $this->price = $price;
    $this->type = $type;
    $this->height = $height;
    $this->width = $width;
    $this->length = $length;
  }
  public function validation($sku, $name, $price, $type, $height, $width, $length)
  {
    if (empty($sku)) {
      $this->errors['sku'] = 'sku is empty';
    }
    if (empty($name)) {
      $this->errors['name'] = 'name is empty';
    }
    if (empty($price)) {
      $this->errors['price'] = 'price is empty';
    }
    if (empty($type)) {
      $this->errors['type'] = 'type is empty';
    }
    if (empty($height)) {
      $this->errors['height'] = 'height is empty';
    }
    if (empty($width)) {
      $this->errors['width'] = 'width is empty';
    }
    if (empty($length)) {
      $this->errors['length'] = 'length is empty';
    }
    if (count($this->errors) > 0) {
      return `Errors Found {$this->errors}`;
    } else return 'ready';
  }
}
class Book extends Product
{
  public $weight;
  private $errors = [];

  public function __construct($sku, $name, $price, $type, $weight)
  {
    $this->sku = $sku;
    $this->name = $name;
    $this->price = $price;
    $this->type = $type;
    $this->weight = $weight;
  }

  public function validation($sku, $name, $price, $type, $weight)
  {
    if (empty($sku)) {
      $this->errors['sku'] = 'sku is empty';
    }
    if (empty($name)) {
      $this->errors['name'] = 'name is empty';
    }
    if (empty($price)) {
      $this->errors['price'] = 'price is empty';
    }
    if (empty($type)) {
      $this->errors['type'] = 'type is empty';
    }
    if (empty($weight)) {
      $this->errors['weight'] = 'weight is empty';
    }
    if (count($this->errors) > 0) {
      return $this->errors;
    } else return 'ready';
  }
}

// $dvd = new Dvd('ee33', 'dvd1', 3, 'dvd', 14);
// $furniture = new Furniture('ee33', 'dvd1', 3, 'dvd', 14, 2, 3);
// $book = new Book('ee33', 'dvd1', 3, 'dvd', 14);
