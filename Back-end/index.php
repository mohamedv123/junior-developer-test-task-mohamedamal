<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Allow-Headers: *");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 1000');
header("Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE");
header("Access-Control-Allow-Headers: Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization");

require_once 'product.php';
require_once  'connection.php';
require_once  'start-connection.php';

$startQuery = new startConnection(new Database(),$_SERVER['REQUEST_METHOD']);
$startQuery->startQuery();
$startQuery->closeConnection();